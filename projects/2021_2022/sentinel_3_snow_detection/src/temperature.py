# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import matplotlib.pyplot as plt
import numpy as np

img = plt.imread(
    "2020-02-24-00 00_2020-02-24-23 59_Sentinel-3_SLSTR_F2_Brightness_Temperature_.png"
)

res = np.zeros((116, 244))
for i in range(116):
    for j in range(244):
        if img[i, j, 2] > 0.8:
            res[i, j] = 4


def scale(img, nR, nC):
    number_rows = len(img)
    number_columns = len(img[0])
    return [
        [
            img[int(number_rows * r / nR)][int(number_columns * c / nC)]
            for c in range(nC)
        ]
        for r in range(nR)
    ]


res = scale(res, 718, 1250)
print(len(res[0]))
plt.imshow(res)
plt.show()

plt.imsave("temperature.png", res)
