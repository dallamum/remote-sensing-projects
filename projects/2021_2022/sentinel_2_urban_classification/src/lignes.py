import sys
import math

import cv2 as cv
import numpy as np


def main(argv):
    default_file = 'pentagon.png'
    filename = argv[0] if len(argv) > 0 else default_file
    # charge une image
    src = cv.imread(cv.samples.findFile(filename), cv.IMREAD_GRAYSCALE)
    # regarde si l'image a bien été chargée
    if src is None:
        print('Error opening image!')
        print('Usage: hough_lines.py [image_name -- default ' + default_file + '] \n')
        return -1
    
    # détecte les bords de l'image grace à un filtre de canny
    dst = cv.Canny(src, 50, 200, None, 3)
    
    cdst = cv.cvtColor(dst, cv.COLOR_GRAY2BGR)
    cdstP = np.copy(cdst)
    
    # Tranformée standard de hough grâce à la bibliothèque opencv
    lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)
    
    # trace les lignes détectées
    if lines is not None:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = math.cos(theta)
            b = math.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000 * a))
            pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000 * a))
            cv.line(cdst, pt1, pt2, (0, 0, 255), 3, cv.LINE_AA)

    # Transformée de Hough probabiliste
    linesP = cv.HoughLinesP(dst, 1, np.pi / 180, 50, None, 50, 10)
    
    if linesP is not None:
        for i in range(0, len(linesP)):
            l = linesP[i][0]
            cv.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0, 0, 255), 3, cv.LINE_AA)
    
    # on affiche les résultats obtenus
    cv.imshow("Image source", src)
    cv.imshow("Lignes détectées - Transformée Standard de Hough", cdst)
    cv.imshow("Lignes détectées - Transformée Probabiliste de Hough", cdstP)
    
    # on attend que l'utilisateur quitte le programme
    cv.waitKey()
    return 0


if __name__ == "__main__":
    main(sys.argv[1:])
