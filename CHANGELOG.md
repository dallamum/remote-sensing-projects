Log changes of the project

# 2023

- Added the projects from the students of the 2021-2022 course
- Added automatic satellite image downloaders

# 2022

- Added a template project for a generic reproducible project structure
- git repository now requires students to fork from the central repository

# 2021
- Started using a centralized git repository for all projects
- Added three new projects
